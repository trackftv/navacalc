﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2017Calculadora
{
    public partial class frmCalculadora : Form
    {
        operationHandler op;
        public frmCalculadora()
        {
            InitializeComponent();
            this.op = new operationHandler();
            this.escribirResultado();
            this.calcDisplay.Focus();
        }

        private void frmCalculadora_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.op.agregarAOperacion((int)e.KeyChar);
            this.escribirResultado();
        }

        private void escribirResultado()
        {
            this.calcDisplay.Text = this.op.darResultado();
        }

        private void buttonClick(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            this.op.agregarAOperacion(Convert.ToInt32(c.Tag.ToString()));
            this.escribirResultado();
        }

        private void frmCalculadora_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 46)
            {
                this.op.agregarAOperacion(127);
                this.escribirResultado();
            }
        }
    }
}
