﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2017Calculadora
{
    class operationHandler
    {
        private int estado, ultimoOperador;
        private bool bloquearPunto;
        private string numeroDisplay;
        private float operando, resultado;
        //127 -> CE
        //27 -> C
        //todos son valores de la tabla ascii
        private int[] teclasValidas = { 127, 27, 8, 13, 42, 43, 45, 46, 47,
            //numeros
            48, 49, 50, 51, 52, 53, 54, 55, 56, 57};

        public operationHandler()
        {
            this.restablecerCalculadora();
        }

        public void agregarAOperacion(int k)
        {
            if (esValido(k))
            {
                if (esNumero(k))
                {
                    if (this.numeroDisplay.Length < 10)
                    {
                        if (this.estado == 0)
                        {
                            if (this.numeroDisplay == "0")
                            {
                                this.numeroDisplay = ((char)k).ToString();
                            }
                            else
                            {
                                this.numeroDisplay += (char)k;
                            }
                        }
                        else
                        {
                            this.numeroDisplay = ((char)k).ToString();
                        }
                        this.estado = 0;
                    }
                }
                else
                {
                    if(k == 46)
                    {//punto
                        if (!this.bloquearPunto)
                        {
                            if(this.estado != 0)
                            {
                                this.numeroDisplay = "0.";
                            }
                            else
                            {
                                this.numeroDisplay += ".";
                            }
                            this.estado = 0;
                            this.bloquearPunto = true;
                        }
                    }
                    else
                    {
                        if (esAritmetica(k))
                        {
                            realizarOperacion(this.ultimoOperador);
                            this.ultimoOperador = k;
                        }
                        else
                        {
                            switch (k)
                            {
                                case 127://CE
                                    this.numeroDisplay = "0";
                                    this.estado = 0;
                                    break;
                                case 27://C
                                    restablecerCalculadora();
                                    break;
                                case 8://retroceso
                                    if (this.numeroDisplay.Length == 1)
                                    {
                                        this.numeroDisplay = "0";
                                    }
                                    else
                                    {
                                        this.numeroDisplay = this.numeroDisplay.Remove(this.numeroDisplay.Length - 1, 1);
                                    }
                                    break;
                                case 13://=
                                    realizarOperacion(this.ultimoOperador);
                                    this.ultimoOperador = 0;
                                    break;
                            }
                        }
                        this.estado = 1;
                    }
                }
            }
        }

        private bool esNumero(int k)
        {
            if (k >= 48 && k <= 57) return true;
            return false;
        }

        private bool esAritmetica(int k)
        {
            if (k == 42 || k == 43 || k == 45 || k == 47) return true;
            return false;
        }

        private bool esValido(int k)
        {
            for(int i = 0; i < teclasValidas.Length; i++)
            {
                if (k == teclasValidas[i]) return true;
            }
            return false;
        }

        private void realizarOperacion(int operador)
        {
            if (this.estado == 1) return;//no realizar una operación si se acaba de realizar otra
            this.operando = (float)Convert.ToDouble(this.numeroDisplay);
            switch (operador)
            {
                case 42://multiplicacion
                    this.resultado *= this.operando;
                    break;
                case 43://suma
                    this.resultado += this.operando;
                    break;
                case 45://resta
                    this.resultado -= this.operando;
                    break;
                case 47://division
                    this.resultado /= this.operando;
                    break;
                default://inicial o igual
                    this.resultado = this.operando;
                    break;
            }
            this.numeroDisplay = this.resultado.ToString();
            this.bloquearPunto = false;
        }

        public string darResultado()
        {
            return this.numeroDisplay;
        }

        public void restablecerCalculadora()
        {
            this.estado = 0;
            this.ultimoOperador = 0;
            this.bloquearPunto = false;
            this.numeroDisplay = "0";
            this.operando = 0;
            this.resultado = 0;
        }
    }
}
